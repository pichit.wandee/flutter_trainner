import 'package:flutter/material.dart';
import '../ui/menu_item.dart';

class MenuScreen  extends StatelessWidget {
  const MenuScreen ({super.key});

  void goTo(BuildContext context, String path){
    Navigator.of(context).pushNamed(path);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: GridView.count(crossAxisCount: 3,
      children: [
        MenuItem(
          label: "User",
          icon: Icons.people,
          color: Colors.lightBlue.shade800,
          onPress: () => goTo(context, '/login'),   
          ),
        MenuItem(
          label: "Todo",
          icon: Icons.list,          
          color: Color.fromARGB(255, 215, 139, 7),
          
          ),
        MenuItem(
          label: "Photo",
          icon: Icons.camera,          
          color: Color.fromARGB(255, 202, 5, 5),
          
          ),
        MenuItem(
          label: "Album",
          icon: Icons.image,
          color: Color.fromARGB(255, 26, 133, 56),
          
          ),
        MenuItem(
          label: "Comment",
          icon: Icons.comment,
          color: Color.fromARGB(255, 236, 125, 7),
          
          ),
        MenuItem(
          label: "Post",
          icon: Icons.post_add,
          color: Color.fromARGB(255, 255, 206, 12),  
          onPress: () => goTo(context, '/post'),        
          ),
      ],
      ),
      
    );
  }
}