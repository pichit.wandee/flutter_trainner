// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unused_field, avoid_print

import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formLogin = GlobalKey<FormState>(); //เพื่อ สั่งให้ตรวจสอบ input
  final _userCode = TextEditingController();
  final _password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset("images/logo_it.png"),
            Form(
              key: _formLogin,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Username',
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        border: OutlineInputBorder(),
                        prefixIcon: Icon(Icons.person),
                        prefixIconColor: Colors.lightBlue.shade800,
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Enter your user account!';
                        }
                        return null;
                      },
                      controller: _userCode,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Password',
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        border: OutlineInputBorder(),
                        prefixIcon: Icon(Icons.key),
                        prefixIconColor: Colors.red.shade800,
                      ),
                      obscureText: true,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Enter your password!';
                        } else if (value.length < 8) {
                          return 'Password must be least 8 characters.';
                        }
                        return null;
                      },
                      controller: _password,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Expanded(
                            child: ElevatedButton(
                                onPressed: () {
                                  _formLogin.currentState!.validate();
                                  print(_userCode.text);
                                  print(_password.text);
                                },
                                child: Text("Login"))),
                        Expanded(
                            child: ElevatedButton(
                                onPressed: () {}, child: Text("Reset"))),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, "/register");
                    },
                    child: Text(
                      "Register your account.",
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.blue,
                        fontStyle: FontStyle.italic,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
