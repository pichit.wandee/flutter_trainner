import 'package:flutter/material.dart';

class MyInput extends StatefulWidget {
  final String label;
  final Icon? icon;
  final Color? iconColor;
  final String? Function(String?)? validator;
  final TextEditingController? controller;
  final bool obscureText;

  const MyInput({super.key, required this.label,
  this.icon, this.iconColor, this.validator,
  this.obscureText = false, this.controller,  
  });

  @override
   State<MyInput> createState() => _MyInputState();
}

class _MyInputState extends State<MyInput> {
  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}